package com.autogen.api.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * TODoItem
 * @author marlonb
 *
 */
@Entity
public class ToDoItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4383219335671282997L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@NotNull(message = "Text field must be defined")
	@Size(min=1, max = 50, message = "Input must be between 1 and 50 chars long")
	private String text;
	private Boolean isIsCompleted;
	private LocalDateTime createdAt;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Boolean isIsCompleted() {
		return isIsCompleted;
	}
	public void setIsCompleted(Boolean isIsCompleted) {
		this.isIsCompleted = isIsCompleted;
	}
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
	
	
	

}
