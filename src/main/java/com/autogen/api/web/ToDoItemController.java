package com.autogen.api.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.autogen.api.entity.ToDoItem;
import com.autogen.api.model.ToDoItemAddRequest;
import com.autogen.api.model.ToDoItemUpdateRequest;
import com.autogen.api.service.ToDoItemService;

/**
 * todo endpoint handler
 *
 * @author marlonb
 *
 */
@RestController
@RequestMapping("todo")
@Validated
public class ToDoItemController {
	@Autowired
	private ToDoItemService toDoItemService;
	
	@GetMapping("/{id}")
	public ToDoItem getById(@PathVariable Long id) {
		return toDoItemService.getToDoItemById(id);
	}
	
	@PostMapping("")
	public ToDoItem create(@Valid @RequestBody ToDoItemAddRequest toDoItemAddRequest) {
		final ToDoItem toDoItem = new ToDoItem();
		toDoItem.setText(toDoItemAddRequest.getText());
		toDoItem.setIsCompleted(Boolean.FALSE);
		return toDoItemService.addToDoItem(toDoItem);
	}
	
	@PatchMapping("/{id}")
	public ToDoItem update(@Valid @RequestBody ToDoItemUpdateRequest toDoItemUpdateRequest, @PathVariable Long id) {
		final ToDoItem toDoItem = new ToDoItem();
		toDoItem.setText(toDoItemUpdateRequest.getText());
		if (toDoItemUpdateRequest.isIsCompleted() != null) {
			toDoItem.setIsCompleted(toDoItemUpdateRequest.isIsCompleted());
		}
		
		return toDoItemService.updateToDoItem(toDoItem, id);
	}

}
