package com.autogen.api.web;

import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.autogen.api.model.BalanceTestResult;
import com.autogen.api.service.TaskService;

/**
 * Task endpoint handler.
 *
 * @author marlonb
 *
 */
@RestController
@RequestMapping("tasks")
@Validated
public class TaskController {
	@Autowired
	private TaskService taskService;
	
	
    @GetMapping("/validateBrackets")
    @ResponseBody
    public BalanceTestResult validateBrackets(@Size(min=1, max = 100, message = "Input must be between 1 and 100 chars long") @RequestParam("input") String input) throws Exception {
        return taskService.validateBrackets(input);
    }
    
}
