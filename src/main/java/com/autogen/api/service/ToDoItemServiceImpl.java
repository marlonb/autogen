package com.autogen.api.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.autogen.api.entity.ToDoItem;
import com.autogen.api.exception.ApiApplicationRuntimeException;
import com.autogen.api.repository.ToDoItemRepository;

/**
 * ToDoItemService implementation
 *
 * @author marlonb
 *
 */
@Service
public class ToDoItemServiceImpl implements ToDoItemService {

	@Autowired
	private ToDoItemRepository toDoItemRepository;
	
	@Override
	public ToDoItem getToDoItemById(long id) {
		return toDoItemRepository.findById(id).orElseThrow(() -> new ApiApplicationRuntimeException("Item with id " + id + " not found"));
	}

	@Override
	public ToDoItem addToDoItem(ToDoItem toDoItem) {
		toDoItem.setCreatedAt(LocalDateTime.now());
		return toDoItemRepository.save(toDoItem);
	}

	@Override
	public ToDoItem updateToDoItem(ToDoItem newToDoItem, long id) {
		return toDoItemRepository.findById(id)
				.map(toDoItem -> {
					//update isCompleted only if defined
					if (newToDoItem.isIsCompleted() != null) {
						toDoItem.setIsCompleted(newToDoItem.isIsCompleted());
					}
					
					toDoItem.setText(newToDoItem.getText());
					
					return toDoItemRepository.save(toDoItem);
				})
				.orElseGet(() -> {return null;});
				
		
	}

	

}
