package com.autogen.api.service;

import org.springframework.stereotype.Service;

import com.autogen.api.model.BalanceTestResult;
import com.autogen.api.util.ApiApplicationUtil;

@Service
public class TaskServiceImpl implements TaskService {
	

	@Override
	public BalanceTestResult validateBrackets(String input) {
		return new BalanceTestResult(input, ApiApplicationUtil.validateBrackets(input));
		
	}
	
	
	
	

}
