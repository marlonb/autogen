package com.autogen.api.service;

import com.autogen.api.entity.ToDoItem;

/**
 * ToDoItem CRUD
 * @author marlonb
 *
 */
public interface ToDoItemService {
	ToDoItem getToDoItemById(long id);
	ToDoItem addToDoItem(ToDoItem toDoItem);
	ToDoItem updateToDoItem(ToDoItem toDoItem, long id);
}
