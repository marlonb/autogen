package com.autogen.api.service;

import com.autogen.api.model.BalanceTestResult;
/**
 * 
 * @author marlonb
 *
 */
public interface TaskService {
	/**
	 * Criteria:
	 * 
	 * 1. For every opening bracket (i.e., (, {, or [), there is a matching 
	 * closing bracket (i.e., ), }, or ]) 
	 * of the same type (i.e., ( matches ), { matches }, and [ matches ]). 
	 * An opening bracket must appear before (to the left of) its 
	 * matching closing bracket. For example, ]{}[ is not balanced.
	 * 
	 * 2. No unmatched braces lie between some pair of matched bracket. 
	 * For example, ({[]}) is balanced, but {[}] and [{)] are not balanced.
	 * 
	 * @param input
	 * @return
	 */
	BalanceTestResult validateBrackets(String input);

}
