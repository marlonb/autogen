package com.autogen.api.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ToDoItemAddRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4470839389301817773L;
	
	@NotBlank(message = "Text field must be defined")
	@Size(min=1, max = 50, message = "Input must be between {min} and {max} chars long")
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	
}
