package com.autogen.api.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ToDoItemUpdateRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6078141765239614798L;

	@NotBlank(message = "Text field must be defined")
	@Size(min=1, max = 50, message = "Input must be between {min} and {max} chars long")
	private String text;
	private Boolean isIsCompleted;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Boolean isIsCompleted() {
		return isIsCompleted;
	}
	public void setIsCompleted(Boolean isIsCompleted) {
		this.isIsCompleted = isIsCompleted;
	}
}
