package com.autogen.api.model;

import java.io.Serializable;

public class BalanceTestResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4169513327628060603L;
	
	private String input;
	private boolean isIsBalanced;
	
	
	
	public BalanceTestResult(String input, boolean isIsBalanced) {
		super();
		this.input = input;
		this.isIsBalanced = isIsBalanced;
	}
	
	
	public String getInput() {
		return input;
	}
	public void setInput(String input) {
		this.input = input;
	}


	public boolean isIsBalanced() {
		return isIsBalanced;
	}


	public void setIsBalanced(boolean isIsBalanced) {
		this.isIsBalanced = isIsBalanced;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((input == null) ? 0 : input.hashCode());
		result = prime * result + (isIsBalanced ? 1231 : 1237);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BalanceTestResult other = (BalanceTestResult) obj;
		if (input == null) {
			if (other.input != null)
				return false;
		} else if (!input.equals(other.input))
			return false;
		if (isIsBalanced != other.isIsBalanced)
			return false;
		return true;
	}


	
	
	
	
	

}
