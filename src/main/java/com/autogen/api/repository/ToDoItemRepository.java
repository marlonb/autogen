package com.autogen.api.repository;

import org.springframework.data.repository.CrudRepository;

import com.autogen.api.entity.ToDoItem;

/**
 * ToDoItem repository
 * @author marlonb
 *
 */
public interface ToDoItemRepository extends CrudRepository<ToDoItem, Long> {


}
