package com.autogen.api.exception;

import java.io.Serializable;

public class ToDoItemNotFoundError implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5318438549285816287L;
	private ToDoItemNotFoundDetail[] details;
	private String name;


	public ToDoItemNotFoundDetail[] getDetails() {
		return details;
	}

	public void setDetails(ToDoItemNotFoundDetail[] details) {
		this.details = details;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public static class ToDoItemNotFoundDetail {
		private String message;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
		
		
	
	}
}
