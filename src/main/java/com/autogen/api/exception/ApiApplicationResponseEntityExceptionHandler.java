package com.autogen.api.exception;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.autogen.api.exception.ToDoItemNotFoundError.ToDoItemNotFoundDetail;

/**
 * App exception handler
 * Converts default exceptions to ApiApplicationError
 *
 * @author marlonb
 *
 */
@ControllerAdvice
public class ApiApplicationResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		final ApiApplicationErrorDetail errorDetail = new ApiApplicationErrorDetail();
		errorDetail.setLocation("params");
		errorDetail.setParam(ex.getParameterName());
		errorDetail.setMsg("Invalid Value");

		final ApiApplicationError error = new ApiApplicationError();
		error.setDetails(new ApiApplicationErrorDetail[] {errorDetail});
		error.setName("ValidationError");


		return new ResponseEntity<>(error, status);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		final Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();

		final Set<ApiApplicationErrorDetail> errorDetails = new HashSet<>(constraintViolations.size());
		errorDetails.addAll(constraintViolations.stream()
				.map(constraintViolation -> {
					ApiApplicationErrorDetail errorDetail = new ApiApplicationErrorDetail();
					errorDetail.setLocation("params");
					errorDetail.setParam(String.valueOf(constraintViolation.getPropertyPath()));
					errorDetail.setMsg(constraintViolation.getMessage());
					errorDetail.setValue(String.valueOf(constraintViolation.getInvalidValue()));


					return errorDetail;
				})
				.collect(Collectors.toList()));

		final ApiApplicationError error = new ApiApplicationError();
		error.setDetails(errorDetails.toArray(new ApiApplicationErrorDetail[] {}));
		error.setName("ValidationError");

		return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ApiApplicationRuntimeException.class)
	public final ResponseEntity<Object> handleToDoItemNotFoundException(ApiApplicationRuntimeException ex, WebRequest request) {
		final ToDoItemNotFoundDetail errorDetail = new ToDoItemNotFoundDetail();
		errorDetail.setMessage(ex.getMessage());

		final ToDoItemNotFoundError error = new ToDoItemNotFoundError();
		error.setDetails(new ToDoItemNotFoundDetail[] {errorDetail});
		error.setName("NotFoundError");

		return new ResponseEntity<Object>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}



	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		final ApiApplicationMethodNotSupportedException error = new ApiApplicationMethodNotSupportedException();
		error.setCode("MethodNotAllowed");
		error.setMessage(ex.getMessage());

		return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		final List<ApiApplicationErrorDetail> errorDetails = new ArrayList<>();
		for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
			final ApiApplicationErrorDetail errorDetail = new ApiApplicationErrorDetail();
			errorDetail.setLocation("params");
			errorDetail.setParam(fieldError.getField());		
			errorDetail.setMsg(fieldError.getDefaultMessage());
			errorDetail.setValue(String.valueOf(fieldError.getRejectedValue()));
			errorDetails.add(errorDetail);
		}


		final ApiApplicationError error = new ApiApplicationError();
		error.setDetails(errorDetails.toArray(new ApiApplicationErrorDetail[] {}));
		error.setName("ValidationError");

		return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
	}
}
