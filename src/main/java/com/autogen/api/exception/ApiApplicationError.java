package com.autogen.api.exception;

public class ApiApplicationError {
	private ApiApplicationErrorDetail[] details;
	private String name;
	
	public ApiApplicationErrorDetail[] getDetails() {
		return details;
	}
	public void setDetails(ApiApplicationErrorDetail[] details) {
		this.details = details;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	

}
