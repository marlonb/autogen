package com.autogen.api.exception;

import java.io.Serializable;

public class ApiApplicationMethodNotSupportedException implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2027644444665941958L;

	private String code;
	private String message;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
