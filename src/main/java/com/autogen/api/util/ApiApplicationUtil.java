package com.autogen.api.util;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Application utility class
 * @author marlonb
 *
 */
public class ApiApplicationUtil {
	private static final String ROUND_BRACKETS_START = "(";
	private static final String ROUND_BRACKETS_END = ")";
	private static final String CURLY_BRACKETS_START = "{";
	private static final String CURLY_BRACKETS_END = "}";
	private static final String SQUARE_BRACKETS_START = "[";
	private static final String SQUARE_BRACKETS_END = "]";
	protected static final String[] ALL_BRACKETS_START = new String[] {ROUND_BRACKETS_START, CURLY_BRACKETS_START, SQUARE_BRACKETS_START};
	protected static final String[] ALL_BRACKETS = new String[] {ROUND_BRACKETS_START, CURLY_BRACKETS_START, SQUARE_BRACKETS_START, ROUND_BRACKETS_END, CURLY_BRACKETS_END, SQUARE_BRACKETS_END};
	
	
	private ApiApplicationUtil() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * Returns true if the brackets in the input are balanced.
	 *
	 * @param input
	 * @return
	 */
	public static boolean validateBrackets(String input) {
		if (input != null && !input.trim().isEmpty()) {
			if (inputStrContainsAnyItemFromArr(input, ALL_BRACKETS)) {
				// If there's any bracket..check if balanced.
				final String newInput = stripAllCharsNotFromArr(input, ALL_BRACKETS);
				if (!newInput.isEmpty()) {
					final LinkedList<String> queue = new LinkedList<>();
					for (int i = 0; i < newInput.length(); i++) {
						final String currentStr = String.valueOf(newInput.charAt(i));
						if (inputStrEqualToAnyItemFromArr(currentStr, ALL_BRACKETS_START)) {
							queue.addLast(currentStr);
						} else {
							final String lastItemFromQueue = queue.peekLast();
							if (lastItemFromQueue != null) {
								if (currentStr.equals(ROUND_BRACKETS_END)
										&& lastItemFromQueue.equals(ROUND_BRACKETS_START)) {
									queue.removeLast();
								} else if (currentStr.equals(CURLY_BRACKETS_END)
										&& lastItemFromQueue.equals(CURLY_BRACKETS_START)) {
									queue.removeLast();
								} else if (currentStr.equals(SQUARE_BRACKETS_END)
										&& lastItemFromQueue.equals(SQUARE_BRACKETS_START)) {
									queue.removeLast();
								} else {
									return false;
								}
							} else {
								return false;
							}							
						}
					}
					return queue.isEmpty();
				}				
			}
		}
		// TODO Auto-generated method stub
		return true;
	}
	
	protected static boolean inputStrEqualToAnyItemFromArr(String inputStr, String[] items) {
	    return Arrays.stream(items).anyMatch(inputStr::equals);
	}
	
	
	protected static boolean inputStrContainsAnyItemFromArr(String inputStr, String[] items) {
	    return Arrays.stream(items).anyMatch(inputStr::contains);
	}
	
	protected static String stripAllCharsNotFromArr(String inputStr, String[] inputArr) {
		return inputStr.replaceAll("[^" + String.join("\\", inputArr) + "]", "");
	}
}
