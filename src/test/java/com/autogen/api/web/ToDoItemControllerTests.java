package com.autogen.api.web;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.autogen.api.config.ApiAppConfig;
import com.autogen.api.entity.ToDoItem;
import com.autogen.api.service.ToDoItemService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ToDoItemController.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = ApiAppConfig.class)
public class ToDoItemControllerTests {
	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
	private ToDoItemService toDoItemService;
	
	@Test
    public void shouldGetById() throws Exception {
		ToDoItem item = new ToDoItem();
		item.setText("sampletext");
		item.setIsCompleted(true);
		item.setCreatedAt(LocalDateTime.now());
		item.setId(1);
		
        when(toDoItemService.getToDoItemById(1)).thenReturn(item);
        this.mockMvc.perform(get("/todo/1").contentType(MediaType.APPLICATION_JSON))
        		.andDo(print()).andExpect(status().isOk())
        		.andExpect(content().string(containsString("\"isCompleted\":true")))
        		.andExpect(content().string(containsString("\"id\":1,\"text\":\"sampletext\",")));
    }
	
	@Test
    public void shouldCreateToDoItem() throws Exception {
		ToDoItem item = new ToDoItem();
		item.setText("sample1");
		item.setIsCompleted(false);
		item.setCreatedAt(LocalDateTime.now());
		item.setId(1);
		
        when(toDoItemService.addToDoItem(any())).thenReturn(item);
        this.mockMvc.perform(post("/todo")
        		.content("{\"text\": \"sample1\",\"isCompleted\": false}")
        		.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))        
        		.andDo(print()).andExpect(status().isOk())
        		.andExpect(content().string(containsString("\"isCompleted\":false")))
        		.andExpect(content().string(containsString("\"id\":1,\"text\":\"sample1\",")));
    }
	
	@Test
    public void shouldPatchToDoItem() throws Exception {
		ToDoItem item = new ToDoItem();
		item.setText("sample1");
		item.setIsCompleted(true);
		item.setCreatedAt(LocalDateTime.now());
		item.setId(1);
		
        when(toDoItemService.updateToDoItem(any(), anyLong())).thenReturn(item);
        this.mockMvc.perform(patch("/todo/1")
        		.content("{\"text\": \"sample1\",\"isCompleted\": true}")
        		.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))        
        		.andDo(print()).andExpect(status().isOk())
        		.andExpect(content().string(containsString("\"isCompleted\":true")))
        		.andExpect(content().string(containsString("\"id\":1,\"text\":\"sample1\",")));
    }
	
	@Test
    public void beanValidationsShouldReturnErrors() throws Exception {
		this.mockMvc.perform(get("/todo").contentType(MediaType.APPLICATION_JSON))
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(content().string(equalTo("{\"code\":\"MethodNotAllowed\",\"message\":\"Request method 'GET' not supported\"}")));
		
		this.mockMvc.perform(post("/todo").contentType(MediaType.APPLICATION_JSON)
				.content("{\"text\": \"\",\"isCompleted\": false}"))		
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(content().string(containsString("\"msg\":\"Input must be between 1 and 50 chars long\"")));
		
		
		this.mockMvc.perform(post("/todo").contentType(MediaType.APPLICATION_JSON)
				.content("{\"isCompleted\": false}"))		
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(content().string(containsString("\"msg\":\"Text field must be defined\"")));
		
		this.mockMvc.perform(patch("/todo").contentType(MediaType.APPLICATION_JSON))
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(content().string(equalTo("{\"code\":\"MethodNotAllowed\",\"message\":\"Request method 'PATCH' not supported\"}")));
		
		
		this.mockMvc.perform(patch("/todo/1").contentType(MediaType.APPLICATION_JSON)
				.content("{\"text\": \"\",\"isCompleted\": false}"))		
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(content().string(containsString("\"msg\":\"Input must be between 1 and 50 chars long\"")));
		
		
		this.mockMvc.perform(patch("/todo/1").contentType(MediaType.APPLICATION_JSON)
				.content("{\"isCompleted\": false}"))		
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(content().string(containsString("\"msg\":\"Text field must be defined\"")));
		
	}

}
