package com.autogen.api.web;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.autogen.api.model.BalanceTestResult;
import com.autogen.api.service.TaskService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TaskController.class)
@ActiveProfiles("test")
public class TaskControllerTests {
	
	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
	private TaskService taskService;
	
	
	@Test
    public void validateBracketsShouldReturnBalanceTestResult() throws Exception {
        when(taskService.validateBrackets("sampleinput")).thenReturn(new BalanceTestResult("sampleinput", true));
        this.mockMvc.perform(get("/tasks/validateBrackets").param("input", "sampleinput").contentType(MediaType.APPLICATION_JSON))
        		.andDo(print()).andExpect(status().isOk())
        		.andExpect(content().string(equalTo("{\"input\":\"sampleinput\",\"isBalanced\":true}")));
    }
	
	@Test
    public void invalidBracketsShouldReturn400() throws Exception {
		this.mockMvc.perform(get("/tasks/validateBrackets").contentType(MediaType.APPLICATION_JSON))
		.andDo(print()).andExpect(status().isBadRequest());
		
		this.mockMvc.perform(get("/tasks/validateBrackets").param("input", "").contentType(MediaType.APPLICATION_JSON))
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(content().string(equalTo("{\"details\":[{\"location\":\"params\",\"param\":\"validateBrackets.input\",\"msg\":\"Input must be between 1 and 100 chars long\",\"value\":\"\"}],\"name\":\"ValidationError\"}")));
		
		this.mockMvc.perform(get("/tasks/validateBrackets").param("input", "stringmorethan100chars_ahshsahsahsahsasksjdsjdshjshjsdhwneheheheqyqyqyqueueiririritotoototototototototkfkfkfkfkamaalaojenwmwkfjfkrkrkrrndnfmcvnhekekeke").contentType(MediaType.APPLICATION_JSON))
		.andDo(print()).andExpect(status().isBadRequest())
		.andExpect(content().string(containsString("\"param\":\"validateBrackets.input\",\"msg\":\"Input must be between 1 and 100 chars long\"")));
	}

}
