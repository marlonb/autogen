package com.autogen.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.autogen.api.config.ApiAppConfig;
import com.autogen.api.entity.ToDoItem;
import com.autogen.api.repository.ToDoItemRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = ApiAppConfig.class)
public class ToDoItemServiceImplTests {
	@Autowired
	private ToDoItemService toDoItemService;
	
	@MockBean
	private ToDoItemRepository toDoItemRepository;
	
	@Before
	public void setUp() {
		final ToDoItem item = new ToDoItem();
		item.setId(1L);
		item.setText("sampleText");
		item.setIsCompleted(true);
		item.setCreatedAt(LocalDateTime.now());
		
		Mockito.when(toDoItemRepository.findById(item.getId())).thenReturn(Optional.of(item));
		Mockito.when(toDoItemRepository.save(any())).thenReturn(item);
	 
	}

	@Test
	public void whenGetById_ShouldReturnToDoItem() {
		ToDoItem item = toDoItemService.getToDoItemById(1L);
		assertThat(item.getText()).isEqualTo("sampleText");

        verifyFindByIdIsCalledOnce();
	}
	
	
	@Test
	public void whenAdd_ShouldReturnToDoItem() {
		ToDoItem item = toDoItemService.addToDoItem(new ToDoItem());
		assertThat(item.getText()).isEqualTo("sampleText");
		assertThat(item.getCreatedAt()).isNotNull();
	}
	
	@Test
	public void whenUpdate_ShouldReturnToDoItem() {
		final ToDoItem newItem = new ToDoItem();
		newItem.setText("updatedText");
		ToDoItem item = toDoItemService.updateToDoItem(newItem, 1);
		assertThat(item.getText()).isEqualTo("updatedText");
		assertThat(item.getCreatedAt()).isNotNull();
	}
	
	
	private void verifyFindByIdIsCalledOnce() {
        Mockito.verify(toDoItemRepository, VerificationModeFactory.times(1)).findById(Mockito.anyLong());
        Mockito.reset(toDoItemRepository);
    }
}
