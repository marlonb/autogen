package com.autogen.api.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.autogen.api.config.ApiAppConfig;
import com.autogen.api.model.BalanceTestResult;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = ApiAppConfig.class)
public class TaskServiceImplTests {
	@Autowired
	private TaskService taskService;
	
	@Test
	public void taskServiceImplShouldBeDefined() {
		assertThat(this.taskService).isNotNull();		
	}
	
	@Test
	public void shouldReturnIsBalancedTrue() {
		assertThat(this.taskService.validateBrackets("stringwithoutbrackets")).isEqualTo(new BalanceTestResult("stringwithoutbrackets", true));
		assertThat(this.taskService.validateBrackets("-+*&^`!%$#absh,./?<>")).isEqualTo(new BalanceTestResult("-+*&^`!%$#absh,./?<>", true));
		assertThat(this.taskService.validateBrackets("(-+*&^`!%$#absh,./?<)>")).isEqualTo(new BalanceTestResult("(-+*&^`!%$#absh,./?<)>", true));
		assertThat(this.taskService.validateBrackets("({[]})")).isEqualTo(new BalanceTestResult("({[]})", true));
		assertThat(this.taskService.validateBrackets("1(2(3)45[6{7}88{9}0{99}8]7(7)4)33")).isEqualTo(new BalanceTestResult("1(2(3)45[6{7}88{9}0{99}8]7(7)4)33", true));
		assertThat(this.taskService.validateBrackets("[{a}{1}{r}]")).isEqualTo(new BalanceTestResult("[{a}{1}{r}]", true));
		assertThat(this.taskService.validateBrackets("[{()}{()}{()}]")).isEqualTo(new BalanceTestResult("[{()}{()}{()}]", true));
		assertThat(this.taskService.validateBrackets("[{([a])}{([b])}{([a])}]")).isEqualTo(new BalanceTestResult("[{([a])}{([b])}{([a])}]", true));
		assertThat(this.taskService.validateBrackets("[(.)(.)]")).isEqualTo(new BalanceTestResult("[(.)(.)]", true));
		assertThat(this.taskService.validateBrackets("\\[()()]\\")).isEqualTo(new BalanceTestResult("\\[()()]\\", true));
		assertThat(this.taskService.validateBrackets("\\[(*)(*)]\\//")).isEqualTo(new BalanceTestResult("\\[(*)(*)]\\//", true));
		assertThat(this.taskService.validateBrackets("a\\[([])({})]\\//b")).isEqualTo(new BalanceTestResult("a\\[([])({})]\\//b", true));
		assertThat(this.taskService.validateBrackets("a[b{c}d{e}f{g}h]iwjekdjsu")).isEqualTo(new BalanceTestResult("a[b{c}d{e}f{g}h]iwjekdjsu", true));
		
			
	}
	
	@Test
	public void shouldReturnIsBalancedFalse() {
		assertThat(this.taskService.validateBrackets("][")).isEqualTo(new BalanceTestResult("][", false));
		assertThat(this.taskService.validateBrackets("]{}[")).isEqualTo(new BalanceTestResult("]{}[", false));
		assertThat(this.taskService.validateBrackets("{[}]")).isEqualTo(new BalanceTestResult("{[}]", false));
		assertThat(this.taskService.validateBrackets("[{)]")).isEqualTo(new BalanceTestResult("[{)]", false));
		assertThat(this.taskService.validateBrackets("[{]")).isEqualTo(new BalanceTestResult("[{]", false));
		assertThat(this.taskService.validateBrackets("[)]")).isEqualTo(new BalanceTestResult("[)]", false));
		assertThat(this.taskService.validateBrackets("[]]")).isEqualTo(new BalanceTestResult("[]]", false));
		assertThat(this.taskService.validateBrackets("[]}")).isEqualTo(new BalanceTestResult("[]}", false));
		assertThat(this.taskService.validateBrackets("{]}")).isEqualTo(new BalanceTestResult("{]}", false));
		assertThat(this.taskService.validateBrackets("{{}")).isEqualTo(new BalanceTestResult("{{}", false));
		assertThat(this.taskService.validateBrackets("{}}")).isEqualTo(new BalanceTestResult("{}}", false));
		assertThat(this.taskService.validateBrackets("[]({)")).isEqualTo(new BalanceTestResult("[]({)", false));
		assertThat(this.taskService.validateBrackets("[")).isEqualTo(new BalanceTestResult("[", false));
		assertThat(this.taskService.validateBrackets("]")).isEqualTo(new BalanceTestResult("]", false));
		assertThat(this.taskService.validateBrackets("}")).isEqualTo(new BalanceTestResult("}", false));
		assertThat(this.taskService.validateBrackets(")")).isEqualTo(new BalanceTestResult(")", false));
		assertThat(this.taskService.validateBrackets("{")).isEqualTo(new BalanceTestResult("{", false));
		assertThat(this.taskService.validateBrackets("(")).isEqualTo(new BalanceTestResult("(", false));
		
			
	}

}
