package com.autogen.api.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.autogen.api.config.ApiAppConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = ApiAppConfig.class)
public class ApiApplicationUtilTests {
	
	@Test
	public void shouldReturnTrueValidBrackets() {
		assertThat(ApiApplicationUtil.validateBrackets("stringwithoutbrackets")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("-+*&^`!%$#absh,./?<>")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("(-+*&^`!%$#absh,./?<)>")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("({[]})")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("1(2(3)45[6{7}88{9}0{99}8]7(7)4)33")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("[{a}{1}{r}]")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("[{()}{()}{()}]")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("[{([a])}{([b])}{([a])}]")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("[(.)(.)]")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("\\[()()]\\")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("\\[(*)(*)]\\//")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("a\\[([])({})]\\//b")).isTrue();
		assertThat(ApiApplicationUtil.validateBrackets("a[b{c}d{e}f{g}h]iwjekdjsu")).isTrue();
	}
	
	@Test
	public void shouldReturnFalseValidBrackets() {
		assertThat(ApiApplicationUtil.validateBrackets("][")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("]{}[")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("{[}]")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("[{)]")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("[{]")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("[)]")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("[]]")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("[]}")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("{]}")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("{{}")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("{}}")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("[]({)")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("[")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("]")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("}")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets(")")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("{")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("(")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("(]")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("(}")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("{)")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("{]")).isFalse();
		assertThat(ApiApplicationUtil.validateBrackets("[)")).isFalse();
	}
	
	@Test
	public void shouldStripChars() {
		final String out = ApiApplicationUtil.stripAllCharsNotFromArr("[a(bc{d])}", ApiApplicationUtil.ALL_BRACKETS_START);
		assertThat(out).isEqualTo("[({");
		
		
	}
	
	@Test
	public void shouldReturnTrueContains() {
		boolean result = ApiApplicationUtil.inputStrContainsAnyItemFromArr("[a(bc{d])}", ApiApplicationUtil.ALL_BRACKETS_START);
		assertThat(result).isTrue();
		
		result = ApiApplicationUtil.inputStrContainsAnyItemFromArr("abcd]", ApiApplicationUtil.ALL_BRACKETS);
		assertThat(result).isTrue();
		
		
	}
	
	@Test
	public void shouldReturnTrueEquals() {
		boolean result = ApiApplicationUtil.inputStrEqualToAnyItemFromArr("(", ApiApplicationUtil.ALL_BRACKETS_START);
		assertThat(result).isTrue();
		
		result = ApiApplicationUtil.inputStrEqualToAnyItemFromArr("}", ApiApplicationUtil.ALL_BRACKETS);
		assertThat(result).isTrue();
		
		
	}
	
	@Test
	public void shouldReturnFalseContains() {
		boolean result = ApiApplicationUtil.inputStrContainsAnyItemFromArr("d])}", ApiApplicationUtil.ALL_BRACKETS_START);
		assertThat(result).isFalse();
		
		result = ApiApplicationUtil.inputStrContainsAnyItemFromArr("abcd>", ApiApplicationUtil.ALL_BRACKETS);
		assertThat(result).isFalse();
		
		
	}
	
	@Test
	public void shouldReturnFalseEquals() {
		boolean result = ApiApplicationUtil.inputStrEqualToAnyItemFromArr("(ab", ApiApplicationUtil.ALL_BRACKETS_START);
		assertThat(result).isFalse();
		
		result = ApiApplicationUtil.inputStrEqualToAnyItemFromArr("}as", ApiApplicationUtil.ALL_BRACKETS);
		assertThat(result).isFalse();
		
		
	}

}
