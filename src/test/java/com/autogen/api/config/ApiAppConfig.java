package com.autogen.api.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.autogen.api.service.ToDoItemService;
import com.autogen.api.service.ToDoItemServiceImpl;

@TestConfiguration
public class ApiAppConfig {
	

	@Bean
	public ToDoItemService toDoItemService() {
		return new ToDoItemServiceImpl();
	}

}
